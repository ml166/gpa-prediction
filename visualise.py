import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

data = pd.read_csv('sat-gpa-data.csv')

y = data ['GPA']
x1 = data ['SAT']

plt.scatter(x1,y)
plt.scatter(x1,y)
plt.xlabel('SAT', fontsize = 20)
plt.ylabel('GPA', fontsize = 20)
plt.show()