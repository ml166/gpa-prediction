# GPA Prediction.
Prediction a student's final GPA based on SAT results obtained at college using Linear Regression.

Dataset obtained from Udemy.

## Setting Up
1. Create a virtual Enviroment, use the resource here to setup one.
https://docs.python.org/3/library/venv.html

2. Activate your virtual enviroment in the terminal.


3. Install the require dependencies under requirements.txt.
Run `pip install -r requirements.txt` or `pip3 install -r requirements.txt` depending on your python version.

4. Run the data description script, `python describe.py`, to access data description.
5. Run the visualisation script, `python visualise.py`, to visualise the data on a scatter plot.
6. Run the regression script, `python regression.py`, to generate the regression constants.
7. Run `python visualise-with-regress.py`, to visualise regression line against the scatter plot.