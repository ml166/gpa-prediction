import pandas as pd
import statsmodels.api as sm
import seaborn as sns
sns.set()

data = pd.read_csv('sat-gpa-data.csv')

y = data ['GPA']
x1 = data ['SAT']

x = sm.add_constant(x1)
results = sm.OLS(y,x).fit()
print(results.summary())